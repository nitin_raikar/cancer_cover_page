import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TextMaskModule } from 'angular2-text-mask';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';
import { HeaderComponent } from './FrontPage_Modules/header/header.component';
import { BenefitsComponent } from './FrontPage_Modules/benefits/benefits.component';
import { TempServiceService } from './Service/temp-service.service';
import { BannerComponent } from './FrontPage_Modules/banner/banner.component';
import { DependOnUsComponent } from './FrontPage_Modules/depend-on-us/depend-on-us.component';
import { ScoreCardComponent } from './FrontPage_Modules/score-card/score-card.component';
import { PayoutOptionsComponent } from './FrontPage_Modules/payout-options/payout-options.component';
import { DisclaimerComponent } from './FrontPage_Modules/disclaimer/disclaimer.component';
import { OtherInfoComponent } from './FrontPage_Modules/other-info/other-info.component';
import { PlusminusComponent } from './Shared/plusminus/plusminus.component';
import { BasicDetailsComponent } from './MainForms/basic-details/basic-details.component';
import { PersonalDetailsComponent } from './MainForms/personal-details/personal-details.component';
import { CancerEbiService } from './Service/cancer-ebi.service';
import { BlowfishEncryptionService } from './Service/blowfish-encryption.service';
import { FlipModule } from 'ngx-flip';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HeaderComponent,
    BenefitsComponent,
    BannerComponent,
    DependOnUsComponent,
    ScoreCardComponent,
    PayoutOptionsComponent,
    DisclaimerComponent,
    OtherInfoComponent,
    PlusminusComponent,
    BasicDetailsComponent,
    PersonalDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    FormsModule,
    TextMaskModule,
    HttpClientModule,
    FlipModule,
    ScrollToModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo: 'landing-page', pathMatch: 'full' },
      { path: 'landing-page', component: LandingPageComponent }
    ],{ useHash: true }) 
  ],
  providers: [TempServiceService,CancerEbiService,BlowfishEncryptionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
