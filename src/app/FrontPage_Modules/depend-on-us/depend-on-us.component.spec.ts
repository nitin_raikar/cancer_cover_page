import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependOnUsComponent } from './depend-on-us.component';

describe('DependOnUsComponent', () => {
  let component: DependOnUsComponent;
  let fixture: ComponentFixture<DependOnUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependOnUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependOnUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
