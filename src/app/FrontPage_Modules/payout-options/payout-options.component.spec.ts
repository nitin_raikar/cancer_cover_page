import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutOptionsComponent } from './payout-options.component';

describe('PayoutOptionsComponent', () => {
  let component: PayoutOptionsComponent;
  let fixture: ComponentFixture<PayoutOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
