import { Component, OnInit } from '@angular/core';
import { TempServiceService } from 'src/app/Service/temp-service.service';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)' }),
        animate(400)
      ]),
    ]),
  ]
})
export class BannerComponent implements OnInit {
  mobileStructure;
  desktopStructure;
  showHideMobile;
  showHideMobilePersonal;
  quoteSection = true;
  constructor(public tempservice:TempServiceService) { }

  ngOnInit() {
    this.tempservice.showHideMobile = false;
    this.tempservice.showHideMobilePersonal = false;
    this.tempservice.premiumAmount = 67;
    if((window.screen.width < 768)){
      console.log("inside 76888888888");
      this.tempservice.mobileStructure = true;
      this.tempservice.desktopStructure = false;
    }else{
      console.log("inside 100000000000");
      this.tempservice.mobileStructure = false;
      this.tempservice.desktopStructure = true;
    }
  }

  getQuote() {
    this.tempservice.showHideMobile = true;
    this.tempservice.flag['showHide']['quoteSection'] = false;
  }
}
