import { Component, OnInit } from '@angular/core';
import { TempServiceService } from 'src/app/Service/temp-service.service';

@Component({
  selector: 'app-plusminus',
  templateUrl: './plusminus.component.html',
  styleUrls: ['./plusminus.component.css']
})
export class PlusminusComponent implements OnInit {
  sumAssured;
  constructor(public tempservice: TempServiceService) { }

  ngOnInit() {
    this.sumAssured = this.tempservice.valueRoundup(
      this.tempservice.Cancer_InputArr['LA1_CancerSumAssured']
    );
  }


  incDec(objPrnt): void {
    let incDecVal = 500000;
    let ipVal;
    ipVal = this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'];
      this.tempservice.userInput['LA1_cancerCover'] = this.sumAssured;
      ipVal = parseInt(ipVal);
    if ('plus' === objPrnt) {
      if (ipVal < 100000) {
        incDecVal = 10000;
      } else if (ipVal < 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) + incDecVal;
    } else if (objPrnt === 'minus') {
      if (ipVal <= 100000) {
        incDecVal = 10000;
      } else if (ipVal <= 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) - incDecVal;
    }
    this.minMaxValue(ipVal, 'cancer');
  }

  onFocusField(evt) {
    console.log("Inside Focus event");
    const fieldValue = evt.target.value;
    this.sumAssured = this.tempservice.getValueFromStr(fieldValue);
    console.log("sadsa",this.sumAssured);
  }

  onBlurField(evt) {
    console.log("Inside Blur event");
    var fieldValue = evt.target.value;
    this.minMaxValue(fieldValue, 'cancer');
    // this.minMaxValue(evt.target.value, 'heart');
    // this.tempStorage.OnchangeDefaultParam('heart cover')
  }

  onKeyUp(evt) {
    console.log("Inside OnKeyup event");
    let v = evt.target.value;
    this.sumAssured = v.replace(/[^0-9]/g, '');
  }

  minMaxValue(ipVal, fieldID) {
    console.log("inside MinMax Function");
    if ('cancer' === fieldID) {
      console.log("Inside Cancer check Fun>>>");
      if (ipVal == "" || ipVal == undefined) {
        console.log(">>>>>>1")
        this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'];
      } else if (ipVal < this.tempservice.constants['MIN_CancerCover']) {
        console.log(">>>>>>2");
        this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = this.tempservice.Cancer_InputArr['LA1_CancerSumAssured']
      }
      else if (ipVal > this.tempservice.constants['MAX_CancerCover']) {
        console.log(">>>>>>3");
        this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = this.tempservice.constants['MAX_CancerCover'];
      } else {
        console.log(">>>>>>4");
        this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = ipVal.toString();
      }
      this.sumAssured = this.tempservice.valueRoundup(this.tempservice.Cancer_InputArr['LA1_CancerSumAssured']);
      console.log("this.sumAssured",this.sumAssured);
    }
    this.tempservice.calculatePremium();
  }
}
