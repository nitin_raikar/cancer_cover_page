import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlusminusComponent } from './plusminus.component';

describe('PlusminusComponent', () => {
  let component: PlusminusComponent;
  let fixture: ComponentFixture<PlusminusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlusminusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlusminusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
