import { Injectable } from '@angular/core';
import {
  PremiumRateHeartSP,
  XRTPremiumRateHeartSP,
  PremiumRateCancerSP,
  XRTPremiumRateCancerSP,
  PremiumRateHeartRP,
  XRTPremiumRateHeartRP,
  PremiumRateCancerRP,
  XRTPremiumRateCancerRP
} from '../models';
@Injectable({
  providedIn: 'root'
})
export class CancerEbiService {
  frequencyYearlyFlag;
  RegularPayFlag;
  ProductInputArr;
  PremiumRateHeartSP = PremiumRateHeartSP;
  XRTPremiumRateHeartSP = XRTPremiumRateHeartSP;
  PremiumRateCancerSP = PremiumRateCancerSP;
  XRTPremiumRateCancerSP = XRTPremiumRateCancerSP;
  PremiumRateHeartRP = PremiumRateHeartRP;
  XRTPremiumRateHeartRP = XRTPremiumRateHeartRP;
  PremiumRateCancerRP = PremiumRateCancerRP;
  XRTPremiumRateCancerRP = XRTPremiumRateCancerRP;
  constructor() { 
    this.frequencyYearlyFlag = false;
    this.RegularPayFlag = true;
  }
  Slab_Heart_SumAssured(SlabAmt) {
    if (SlabAmt < 500000) {
      SlabAmt = 200000;
    } else if (SlabAmt < 1000000) {
      SlabAmt = 500000;
    } else if (SlabAmt < 1500000) {
      SlabAmt = 1000000;
    } else if (SlabAmt < 2000000) {
      SlabAmt = 1500000;
    } else {
      SlabAmt = 2000000;
    }
    return SlabAmt;
  }

  Slab_Cancer_SumAssured(SlabAmt) {
    if (SlabAmt < 500000) {
      SlabAmt = 200000;
    } else if (SlabAmt < 1000000) {
      SlabAmt = 200000;
    } else if (SlabAmt < 1500000) {
      SlabAmt = 1000000;
    } else if (SlabAmt < 2000000) {
      SlabAmt = 1500000;
    } else if (SlabAmt < 2500000) {
      SlabAmt = 2000000;
    } else if (SlabAmt < 3000000) {
      SlabAmt = 2500000;
    } else if (SlabAmt < 4000000) {
      SlabAmt = 3000000;
    } else if (SlabAmt < 5000000) {
      SlabAmt = 4000000;
    } else {
      SlabAmt = 4000000;
    }
    return SlabAmt;
  }

  Slab_HeartSumAssured_RP(SlabAmt) {
    if (SlabAmt >= 200000 && SlabAmt < 500000) SlabAmt = 1;
    else if (SlabAmt >= 500000 && SlabAmt < 1000000) SlabAmt = 2;
    else if (SlabAmt >= 1000000 && SlabAmt < 1500000) SlabAmt = 3;
    else if (SlabAmt >= 1500000 && SlabAmt < 2000000) SlabAmt = 4;
    else SlabAmt = 5;
    return SlabAmt;
  }

  Slab_CancerSumAssured_RP(SlabAmt) {
    if (SlabAmt >= 200000 && SlabAmt < 1000000) SlabAmt = 1;
    else if (SlabAmt >= 1000000 && SlabAmt < 1500000) SlabAmt = 2;
    else if (SlabAmt >= 1500000 && SlabAmt < 2000000) SlabAmt = 3;
    else if (SlabAmt >= 2000000 && SlabAmt < 2500000) SlabAmt = 4;
    else if (SlabAmt >= 2500000 && SlabAmt < 3000000) SlabAmt = 5;
    else if (SlabAmt >= 3000000 && SlabAmt < 4000000) SlabAmt = 6;
    else SlabAmt = 7;
    return SlabAmt;
  }

  Trunc_Exel(Amt) {
    return parseFloat(Amt.toString().match(/^-?\d+(?:\.\d{0,3})?/)[0]);
  }
  calculateAge(dob) {
    var today = new Date();
    var birthDate = new Date(
      dob.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3')
    );
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  HeartCancerEBI(HeartCancerObject) {
    this.ProductInputArr = HeartCancerObject;
    var LA1_Age = this.calculateAge(this.ProductInputArr['LA1_DOB']);
    var LA2_Age = this.calculateAge(this.ProductInputArr['LA2_DOB']);
    var Modalloadings;
    var FrequencyDiv;
    if (this.ProductInputArr['PPT'] == 'Single Pay') {
      Modalloadings = 0;
      FrequencyDiv = 1;
    } else {
      if (this.ProductInputArr['Frequency'] == 'Yearly') {
        Modalloadings = 0;
        FrequencyDiv = 1;
      } else if (this.ProductInputArr['Frequency'] == 'Half-Yearly') {
        Modalloadings = 0.035;
        FrequencyDiv = 0.5;
      } else {
        Modalloadings = 0.06;
        FrequencyDiv = 0.0833;
      }
    }
    var index_gender;
    if (this.ProductInputArr['LA1_Gender'] == 'Male') {
      index_gender = 0;
    } else {
      index_gender = 1;
    }

    var SinglePay_HeartArr = [];
    var SinglePay_CancerArr = [];
    var RegularPay_HeartArr = [];
    var RegularPay_CancerArr = [];

    var Base_HeartArr = [];
    var Base_CancerArr = [];
    for (var i = 0; i < 4; i++) {
      var VlookupString =
        LA1_Age.toString() +
        this.Slab_Heart_SumAssured(
          this.ProductInputArr['LA1_HeartSumAssured']
        ).toString();
        console.log();
      var SinglePay_HeartBaseRate = this.PremiumRateHeartSP[VlookupString][
        i * 2 + index_gender
      ];
      SinglePay_HeartArr.push(SinglePay_HeartBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.Slab_Cancer_SumAssured(
          this.ProductInputArr['LA1_CancerSumAssured']
        ).toString();
      var SinglePay_CancerBaseRate = this.PremiumRateCancerSP[VlookupString][
        i * 2 + index_gender
      ];
      SinglePay_CancerArr.push(SinglePay_CancerBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.ProductInputArr['PolicyTerm'] +
        this.Slab_HeartSumAssured_RP(
          this.ProductInputArr['LA1_HeartSumAssured']
        ).toString();
      var RegularPay_HeartBaseRate = this.PremiumRateHeartRP[VlookupString][
        i * 2 + index_gender
      ];
      RegularPay_HeartArr.push(RegularPay_HeartBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.ProductInputArr['PolicyTerm'] +
        this.Slab_CancerSumAssured_RP(
          this.ProductInputArr['LA1_CancerSumAssured']
        ).toString();
      var RegularPay_CancerBaseRate = this.PremiumRateCancerRP[VlookupString][
        i * 2 + index_gender
      ];
      RegularPay_CancerArr.push(RegularPay_CancerBaseRate);

      if (this.ProductInputArr['PPT'] == 'Single Pay') {
        Base_HeartArr.push(SinglePay_HeartArr[i]);
      } else {
        Base_HeartArr.push(RegularPay_HeartArr[i]);
      }

      if (this.ProductInputArr['PPT'] == 'Single Pay') {
        Base_CancerArr.push(SinglePay_CancerArr[i]);
      } else {
        Base_CancerArr.push(RegularPay_CancerArr[i]);
      }
    }

    /* Discount calculation */
    var Staff_disc;
    if (this.ProductInputArr['Staff'] == 'Yes')
      if (this.ProductInputArr['PPT'] == 'Single Pay') Staff_disc = 0.02;
      else if (parseInt(this.ProductInputArr['PolicyTerm']) < 10)
        Staff_disc = 0.05;
      else Staff_disc = 0.075;
    else Staff_disc = 0;

    var Online_disc;
    if (Staff_disc == 0 && this.ProductInputArr['SalesChannel'] == 'Online')
      if (this.ProductInputArr['PPT'] == 'Single Pay') Online_disc = 0.01;
      else Online_disc = 0.02;
    else Online_disc = 0;

    var Loyalty_disc;
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      Staff_disc == 0 &&
      this.ProductInputArr['FamilyBenefit'] == 'No' &&
      this.ProductInputArr['LoyaltyBenefit'] == 'Yes'
    )
      Loyalty_disc = 0.05;
    else Loyalty_disc = 0;

    var Family_disc;
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      Staff_disc == 0 &&
      this.ProductInputArr['FamilyBenefit'] == 'Yes'
    )
      Family_disc = 0.05;
    else Family_disc = 0;
    /* Discount calculation */

    /*	Heart Premium	*/
    var Base_FirstPremium;
    var HospitalBenefit_FirstPremium;
    var IncreasingCoverBenefit_FirstPremium;
    var IncomeBenefit_FirstPremium;
    var HospitalBenefit_ShowPremium;
    var IncreasingCoverBenefit_ShowPremium;
    var IncomeBenefit_ShowPremium;
    var Base_SecondPremium;
    var HospitalBenefit_SecondPremium;
    var IncreasingCoverBenefit_SecondPremium;
    var IncomeBenefit_SecondPremium;

    var ComboCoverDiscount;
    if (
      this.ProductInputArr['CoverageOption'] == 'CancerAndHeart' &&
      this.ProductInputArr['PPT'] == 'Single Pay'
    )
      ComboCoverDiscount = 0.01;
    else if (
      this.ProductInputArr['CoverageOption'] == 'CancerAndHeart' &&
      this.ProductInputArr['PPT'] == 'Regular Pay'
    )
      ComboCoverDiscount = 0.02;
    else ComboCoverDiscount = 0;

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_HeartArr[1] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_HeartArr[0] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    HospitalBenefit_ShowPremium = HospitalBenefit_FirstPremium;
    if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
      HospitalBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_HeartArr[2] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    IncreasingCoverBenefit_ShowPremium = IncreasingCoverBenefit_FirstPremium;
    if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
      IncreasingCoverBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_HeartArr[3] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    IncomeBenefit_ShowPremium = IncomeBenefit_FirstPremium;
    if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
      IncomeBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_HeartArr[1] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
      1000
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Staff_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

    if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      HospitalBenefit_SecondPremium = 0;
    }

    if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      IncreasingCoverBenefit_SecondPremium = 0;
    }

    if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      IncomeBenefit_SecondPremium = 0;
    }
    /*	Heart Premium	*/

    /*	Cancer Premium	*/
    var Cancer_Base_FirstPremium;
    var Cancer_HospitalBenefit_FirstPremium;
    var Cancer_IncreasingCoverBenefit_FirstPremium;
    var Cancer_IncomeBenefit_ShowPremium;
    var Cancer_HospitalBenefit_ShowPremium;
    var Cancer_IncreasingCoverBenefit_ShowPremium;
    var Cancer_IncomeBenefit_FirstPremium;
    var Cancer_Base_SecondPremium;
    var Cancer_HospitalBenefit_SecondPremium;
    var Cancer_IncreasingCoverBenefit_SecondPremium;
    var Cancer_IncomeBenefit_SecondPremium;

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_CancerArr[1] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Cancer_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_CancerArr[0] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Cancer_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    Cancer_HospitalBenefit_ShowPremium = Cancer_HospitalBenefit_FirstPremium;
    if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
      Cancer_HospitalBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_CancerArr[2] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Cancer_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    Cancer_IncreasingCoverBenefit_ShowPremium = Cancer_IncreasingCoverBenefit_FirstPremium;
    if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
      Cancer_IncreasingCoverBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_CancerArr[3] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
      1000
    );
    var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
    );
    var PremiumAfterFamilyBenefit = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 - Family_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterFamilyBenefit * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Cancer_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

    Cancer_IncomeBenefit_ShowPremium = Cancer_IncomeBenefit_FirstPremium;
    if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
      Cancer_IncomeBenefit_FirstPremium = 0;
    }

    var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
      Base_CancerArr[1] * (1 - ComboCoverDiscount)
    );
    var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
      (PremiumRateAfterComboCoverDiscount *
        parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
      1000
    );
    var PremiumAfterStaffDiscount = this.Trunc_Exel(
      PremiumAfterComboCoverDiscount * (1 - Staff_disc)
    );
    var PremiumAfterModalLoading = this.Trunc_Exel(
      PremiumAfterStaffDiscount * (1 + Modalloadings)
    );
    var PremiumAfterOnlineDiscount = this.Trunc_Exel(
      PremiumAfterModalLoading * (1 - Online_disc)
    );
    var PremiumAfterOnlineDiscount_Round =
      Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
    Cancer_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

    if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      Cancer_HospitalBenefit_SecondPremium = 0;
    }

    if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      Cancer_IncreasingCoverBenefit_SecondPremium = 0;
    }

    if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
    } else {
      Cancer_IncomeBenefit_SecondPremium = 0;
    }
    /*	Cancer Premium	*/

    /*	LA2 Calculations	*/
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      this.ProductInputArr['FamilyBenefit'] == 'Yes'
    ) {
      var index_gender;
      if (this.ProductInputArr['LA2_Gender'] == 'Male') {
        index_gender = 0;
      } else {
        index_gender = 1;
      }

      var SinglePay_HeartArr = [];
      var SinglePay_CancerArr = [];
      var RegularPay_HeartArr = [];
      var RegularPay_CancerArr = [];

      var Base_HeartArr = [];
      var Base_CancerArr = [];
      for (var i = 0; i < 4; i++) {
        var VlookupString =
          LA2_Age.toString() +
          this.Slab_Heart_SumAssured(
            this.ProductInputArr['LA2_HeartSumAssured']
          ).toString();
        var SinglePay_HeartBaseRate = this.PremiumRateHeartSP[VlookupString][
          i * 2 + index_gender
        ];
        SinglePay_HeartArr.push(SinglePay_HeartBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.Slab_Cancer_SumAssured(
            this.ProductInputArr['LA2_CancerSumAssured']
          ).toString();
        var SinglePay_CancerBaseRate = this.PremiumRateCancerSP[VlookupString][
          i * 2 + index_gender
        ];
        SinglePay_CancerArr.push(SinglePay_CancerBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.ProductInputArr['PolicyTerm'] +
          this.Slab_HeartSumAssured_RP(
            this.ProductInputArr['LA2_HeartSumAssured']
          ).toString();
        var RegularPay_HeartBaseRate = this.PremiumRateHeartRP[VlookupString][
          i * 2 + index_gender
        ];
        RegularPay_HeartArr.push(RegularPay_HeartBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.ProductInputArr['PolicyTerm'] +
          this.Slab_CancerSumAssured_RP(
            this.ProductInputArr['LA2_CancerSumAssured']
          ).toString();
        var RegularPay_CancerBaseRate = this.PremiumRateCancerRP[VlookupString][
          i * 2 + index_gender
        ];
        RegularPay_CancerArr.push(RegularPay_CancerBaseRate);

        if (this.ProductInputArr['PPT'] == 'Single Pay') {
          Base_HeartArr.push(SinglePay_HeartArr[i]);
        } else {
          Base_HeartArr.push(RegularPay_HeartArr[i]);
        }

        if (this.ProductInputArr['PPT'] == 'Single Pay') {
          Base_CancerArr.push(SinglePay_CancerArr[i]);
        } else {
          Base_CancerArr.push(RegularPay_CancerArr[i]);
        }
      }

      /* Discount calculation */
      var Staff_disc;
      if (this.ProductInputArr['Staff'] == 'Yes')
        if (this.ProductInputArr['PPT'] == 'Single Pay') Staff_disc = 0.02;
        else if (parseInt(this.ProductInputArr['PolicyTerm']) < 10)
          Staff_disc = 0.05;
        else Staff_disc = 0.075;
      else Staff_disc = 0;

      var Online_disc;
      if (Staff_disc == 0 && this.ProductInputArr['SalesChannel'] == 'Online')
        if (this.ProductInputArr['PPT'] == 'Single Pay') Online_disc = 0.01;
        else Online_disc = 0.02;
      else Online_disc = 0;

      var Loyalty_disc;
      if (
        Staff_disc == 0 &&
        this.ProductInputArr['FamilyBenefit'] == 'No' &&
        this.ProductInputArr['LoyaltyBenefit'] == 'Yes'
      )
        Loyalty_disc = 0.05;
      else Loyalty_disc = 0;

      var Family_disc;
      if (Staff_disc == 0 && this.ProductInputArr['FamilyBenefit'] == 'Yes')
        Family_disc = 0.05;
      else Family_disc = 0;
      /* Discount calculation */

      /*	Heart Premium	*/
      var LA2_Base_FirstPremium;
      var LA2_HospitalBenefit_FirstPremium;
      var LA2_IncreasingCoverBenefit_FirstPremium;
      var LA2_IncomeBenefit_FirstPremium;
      var LA2_HospitalBenefit_ShowPremium;
      var LA2_IncreasingCoverBenefit_ShowPremium;
      var LA2_IncomeBenefit_ShowPremium;
      var LA2_Base_SecondPremium;
      var LA2_HospitalBenefit_SecondPremium;
      var LA2_IncreasingCoverBenefit_SecondPremium;
      var LA2_IncomeBenefit_SecondPremium;

      var ComboCoverDiscount;
      if (
        this.ProductInputArr['CoverageOption'] == 'CancerAndHeart' &&
        this.ProductInputArr['PPT'] == 'Single Pay'
      )
        ComboCoverDiscount = 0.01;
      else if (
        this.ProductInputArr['CoverageOption'] == 'CancerAndHeart' &&
        this.ProductInputArr['PPT'] == 'Regular Pay'
      )
        ComboCoverDiscount = 0.02;
      else ComboCoverDiscount = 0;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_HospitalBenefit_ShowPremium = LA2_HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        LA2_HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_IncreasingCoverBenefit_ShowPremium = LA2_IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        LA2_IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_IncomeBenefit_ShowPremium = LA2_IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        LA2_IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_IncomeBenefit_SecondPremium = 0;
      }
      /*	Heart Premium	*/

      /*	Cancer Premium	*/
      var LA2_Cancer_Base_FirstPremium;
      var LA2_Cancer_HospitalBenefit_FirstPremium;
      var LA2_Cancer_IncreasingCoverBenefit_FirstPremium;
      var LA2_Cancer_IncomeBenefit_FirstPremium;
      var LA2_Cancer_HospitalBenefit_ShowPremium;
      var LA2_Cancer_IncreasingCoverBenefit_ShowPremium;
      var LA2_Cancer_IncomeBenefit_ShowPremium;
      var LA2_Cancer_Base_SecondPremium;
      var LA2_Cancer_HospitalBenefit_SecondPremium;
      var LA2_Cancer_IncreasingCoverBenefit_SecondPremium;
      var LA2_Cancer_IncomeBenefit_SecondPremium;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_HospitalBenefit_ShowPremium = LA2_Cancer_HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        LA2_Cancer_HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_IncreasingCoverBenefit_ShowPremium = LA2_Cancer_IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        LA2_Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_IncomeBenefit_ShowPremium = LA2_Cancer_IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        LA2_Cancer_IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_IncomeBenefit_SecondPremium = 0;
      }
      /*	Cancer Premium	*/
    } else {
      LA2_Base_FirstPremium = 0;
      LA2_HospitalBenefit_FirstPremium = 0;
      LA2_IncreasingCoverBenefit_FirstPremium = 0;
      LA2_IncomeBenefit_FirstPremium = 0;
      LA2_HospitalBenefit_ShowPremium = 0;
      LA2_IncreasingCoverBenefit_ShowPremium = 0;
      LA2_IncomeBenefit_ShowPremium = 0;
      LA2_Base_SecondPremium = 0;
      LA2_HospitalBenefit_SecondPremium = 0;
      LA2_IncreasingCoverBenefit_SecondPremium = 0;
      LA2_IncomeBenefit_SecondPremium = 0;
      LA2_Cancer_Base_FirstPremium = 0;
      LA2_Cancer_HospitalBenefit_FirstPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      LA2_Cancer_IncomeBenefit_FirstPremium = 0;
      LA2_Cancer_HospitalBenefit_ShowPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_ShowPremium = 0;
      LA2_Cancer_IncomeBenefit_ShowPremium = 0;
      LA2_Cancer_Base_SecondPremium = 0;
      LA2_Cancer_HospitalBenefit_SecondPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      LA2_Cancer_IncomeBenefit_SecondPremium = 0;
    }
    /*	LA2 Calculations	*/

    /*	XRT	*/
    var XRT_Premium_Rate = this.XRTHeartCancerEBI(HeartCancerObject);
    /*	XRT	*/

    var output_arr = {};
    output_arr = {
      Base_FirstPremium: Base_FirstPremium,
      HospitalBenefit_FirstPremium: HospitalBenefit_FirstPremium,
      IncreasingCoverBenefit_FirstPremium: IncreasingCoverBenefit_FirstPremium,
      IncomeBenefit_FirstPremium: IncomeBenefit_FirstPremium,
      HospitalBenefit_ShowPremium: HospitalBenefit_ShowPremium,
      IncreasingCoverBenefit_ShowPremium: IncreasingCoverBenefit_ShowPremium,
      IncomeBenefit_ShowPremium: IncomeBenefit_ShowPremium,
      Base_SecondPremium: Base_SecondPremium,
      HospitalBenefit_SecondPremium: HospitalBenefit_SecondPremium,
      IncreasingCoverBenefit_SecondPremium: IncreasingCoverBenefit_SecondPremium,
      IncomeBenefit_SecondPremium: IncomeBenefit_SecondPremium,
      Cancer_Base_FirstPremium: Cancer_Base_FirstPremium,
      Cancer_HospitalBenefit_FirstPremium: Cancer_HospitalBenefit_FirstPremium,
      Cancer_IncreasingCoverBenefit_FirstPremium: Cancer_IncreasingCoverBenefit_FirstPremium,
      Cancer_IncomeBenefit_FirstPremium: Cancer_IncomeBenefit_FirstPremium,
      Cancer_HospitalBenefit_ShowPremium: Cancer_HospitalBenefit_ShowPremium,
      Cancer_IncreasingCoverBenefit_ShowPremium: Cancer_IncreasingCoverBenefit_ShowPremium,
      Cancer_IncomeBenefit_ShowPremium: Cancer_IncomeBenefit_ShowPremium,
      Cancer_Base_SecondPremium: Cancer_Base_SecondPremium,
      Cancer_HospitalBenefit_SecondPremium: Cancer_HospitalBenefit_SecondPremium,
      Cancer_IncreasingCoverBenefit_SecondPremium: Cancer_IncreasingCoverBenefit_SecondPremium,
      Cancer_IncomeBenefit_SecondPremium: Cancer_IncomeBenefit_SecondPremium,
      LA2_Base_FirstPremium: LA2_Base_FirstPremium,
      LA2_HospitalBenefit_FirstPremium: LA2_HospitalBenefit_FirstPremium,
      LA2_IncreasingCoverBenefit_FirstPremium: LA2_IncreasingCoverBenefit_FirstPremium,
      LA2_IncomeBenefit_FirstPremium: LA2_IncomeBenefit_FirstPremium,
      LA2_HospitalBenefit_ShowPremium: LA2_HospitalBenefit_ShowPremium,
      LA2_IncreasingCoverBenefit_ShowPremium: LA2_IncreasingCoverBenefit_ShowPremium,
      LA2_IncomeBenefit_ShowPremium: LA2_IncomeBenefit_ShowPremium,
      LA2_Base_SecondPremium: LA2_Base_SecondPremium,
      LA2_HospitalBenefit_SecondPremium: LA2_HospitalBenefit_SecondPremium,
      LA2_IncreasingCoverBenefit_SecondPremium: LA2_IncreasingCoverBenefit_SecondPremium,
      LA2_IncomeBenefit_SecondPremium: LA2_IncomeBenefit_SecondPremium,
      LA2_Cancer_Base_FirstPremium: LA2_Cancer_Base_FirstPremium,
      LA2_Cancer_HospitalBenefit_FirstPremium: LA2_Cancer_HospitalBenefit_FirstPremium,
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium: LA2_Cancer_IncreasingCoverBenefit_FirstPremium,
      LA2_Cancer_IncomeBenefit_FirstPremium: LA2_Cancer_IncomeBenefit_FirstPremium,
      LA2_Cancer_HospitalBenefit_ShowPremium: LA2_Cancer_HospitalBenefit_ShowPremium,
      LA2_Cancer_IncreasingCoverBenefit_ShowPremium: LA2_Cancer_IncreasingCoverBenefit_ShowPremium,
      LA2_Cancer_IncomeBenefit_ShowPremium: LA2_Cancer_IncomeBenefit_ShowPremium,
      LA2_Cancer_Base_SecondPremium: LA2_Cancer_Base_SecondPremium,
      LA2_Cancer_HospitalBenefit_SecondPremium: LA2_Cancer_HospitalBenefit_SecondPremium,
      LA2_Cancer_IncreasingCoverBenefit_SecondPremium: LA2_Cancer_IncreasingCoverBenefit_SecondPremium,
      LA2_Cancer_IncomeBenefit_SecondPremium: LA2_Cancer_IncomeBenefit_SecondPremium,
      XRT_Base_FirstPremium: XRT_Premium_Rate['Base_FirstPremium'],
      XRT_HospitalBenefit_FirstPremium:
        XRT_Premium_Rate['HospitalBenefit_FirstPremium'],
      XRT_IncreasingCoverBenefit_FirstPremium:
        XRT_Premium_Rate['IncreasingCoverBenefit_FirstPremium'],
      XRT_IncomeBenefit_FirstPremium:
        XRT_Premium_Rate['IncomeBenefit_FirstPremium'],
      XRT_Base_SecondPremium: XRT_Premium_Rate['Base_SecondPremium'],
      XRT_HospitalBenefit_SecondPremium:
        XRT_Premium_Rate['HospitalBenefit_SecondPremium'],
      XRT_IncreasingCoverBenefit_SecondPremium:
        XRT_Premium_Rate['IncreasingCoverBenefit_SecondPremium'],
      XRT_IncomeBenefit_SecondPremium:
        XRT_Premium_Rate['IncomeBenefit_SecondPremium'],
      XRT_Cancer_Base_FirstPremium:
        XRT_Premium_Rate['Cancer_Base_FirstPremium'],
      XRT_Cancer_HospitalBenefit_FirstPremium:
        XRT_Premium_Rate['Cancer_HospitalBenefit_FirstPremium'],
      XRT_Cancer_IncreasingCoverBenefit_FirstPremium:
        XRT_Premium_Rate['Cancer_IncreasingCoverBenefit_FirstPremium'],
      XRT_Cancer_IncomeBenefit_FirstPremium:
        XRT_Premium_Rate['Cancer_IncomeBenefit_FirstPremium'],
      XRT_Cancer_Base_SecondPremium:
        XRT_Premium_Rate['Cancer_Base_SecondPremium'],
      XRT_Cancer_HospitalBenefit_SecondPremium:
        XRT_Premium_Rate['Cancer_HospitalBenefit_SecondPremium'],
      XRT_Cancer_IncreasingCoverBenefit_SecondPremium:
        XRT_Premium_Rate['Cancer_IncreasingCoverBenefit_SecondPremium'],
      XRT_Cancer_IncomeBenefit_SecondPremium:
        XRT_Premium_Rate['Cancer_IncomeBenefit_SecondPremium'],
      XRT_LA2_Base_FirstPremium: XRT_Premium_Rate['LA2_Base_FirstPremium'],
      XRT_LA2_HospitalBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_HospitalBenefit_FirstPremium'],
      XRT_LA2_IncreasingCoverBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_IncreasingCoverBenefit_FirstPremium'],
      XRT_LA2_IncomeBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_IncomeBenefit_FirstPremium'],
      XRT_LA2_Base_SecondPremium: XRT_Premium_Rate['LA2_Base_SecondPremium'],
      XRT_LA2_HospitalBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_HospitalBenefit_SecondPremium'],
      XRT_LA2_IncreasingCoverBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_IncreasingCoverBenefit_SecondPremium'],
      XRT_LA2_IncomeBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_IncomeBenefit_SecondPremium'],
      XRT_LA2_Cancer_Base_FirstPremium:
        XRT_Premium_Rate['LA2_Cancer_Base_FirstPremium'],
      XRT_LA2_Cancer_HospitalBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_Cancer_HospitalBenefit_FirstPremium'],
      XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_Cancer_IncreasingCoverBenefit_FirstPremium'],
      XRT_LA2_Cancer_IncomeBenefit_FirstPremium:
        XRT_Premium_Rate['LA2_Cancer_IncomeBenefit_FirstPremium'],
      XRT_LA2_Cancer_Base_SecondPremium:
        XRT_Premium_Rate['LA2_Cancer_Base_SecondPremium'],
      XRT_LA2_Cancer_HospitalBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_Cancer_HospitalBenefit_SecondPremium'],
      XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_Cancer_IncreasingCoverBenefit_SecondPremium'],
      XRT_LA2_Cancer_IncomeBenefit_SecondPremium:
        XRT_Premium_Rate['LA2_Cancer_IncomeBenefit_SecondPremium']
    };
    return output_arr;
    // return this.calculateEBI(HeartCancerObject,output_arr);
  }

  XRTHeartCancerEBI(HeartCancerObject) {
    this.ProductInputArr = HeartCancerObject;
    var LA1_Age = this.calculateAge(this.ProductInputArr['LA1_DOB']);
    var LA2_Age = this.calculateAge(this.ProductInputArr['LA2_DOB']);
    var Modalloadings;
    var FrequencyDiv;
    if (this.ProductInputArr['PPT'] == 'Single Pay') {
      Modalloadings = 0;
      FrequencyDiv = 1;
    } else {
      if (this.ProductInputArr['Frequency'] == 'Yearly') {
        Modalloadings = 0;
        FrequencyDiv = 1;
      } else if (this.ProductInputArr['Frequency'] == 'Half-Yearly') {
        Modalloadings = 0.035;
        FrequencyDiv = 0.5;
      } else {
        Modalloadings = 0.06;
        FrequencyDiv = 0.0833;
      }
    }
    var index_gender;
    if (this.ProductInputArr['LA1_Gender'] == 'Male') {
      index_gender = 0;
    } else {
      index_gender = 1;
    }

    var SinglePay_HeartArr = [];
    var SinglePay_CancerArr = [];
    var RegularPay_HeartArr = [];
    var RegularPay_CancerArr = [];

    var Base_HeartArr = [];
    var Base_CancerArr = [];
    for (var i = 0; i < 4; i++) {
      var VlookupString =
        LA1_Age.toString() +
        this.Slab_Heart_SumAssured(
          this.ProductInputArr['LA1_HeartSumAssured']
        ).toString();
      var SinglePay_HeartBaseRate = this.XRTPremiumRateHeartSP[VlookupString][
        i * 2 + index_gender
      ];
      SinglePay_HeartArr.push(SinglePay_HeartBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.Slab_Cancer_SumAssured(
          this.ProductInputArr['LA1_CancerSumAssured']
        ).toString();
      var SinglePay_CancerBaseRate = this.XRTPremiumRateCancerSP[VlookupString][
        i * 2 + index_gender
      ];
      SinglePay_CancerArr.push(SinglePay_CancerBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.ProductInputArr['PolicyTerm'] +
        this.Slab_HeartSumAssured_RP(
          this.ProductInputArr['LA1_HeartSumAssured']
        ).toString();
      var RegularPay_HeartBaseRate = this.XRTPremiumRateHeartRP[VlookupString][
        i * 2 + index_gender
      ];
      RegularPay_HeartArr.push(RegularPay_HeartBaseRate);

      var VlookupString =
        LA1_Age.toString() +
        this.ProductInputArr['PolicyTerm'] +
        this.Slab_CancerSumAssured_RP(
          this.ProductInputArr['LA1_CancerSumAssured']
        ).toString();
      var RegularPay_CancerBaseRate = this.XRTPremiumRateCancerRP[
        VlookupString
      ][i * 2 + index_gender];
      RegularPay_CancerArr.push(RegularPay_CancerBaseRate);

      if (this.ProductInputArr['PPT'] == 'Single Pay') {
        Base_HeartArr.push(SinglePay_HeartArr[i]);
      } else {
        Base_HeartArr.push(RegularPay_HeartArr[i]);
      }

      if (this.ProductInputArr['PPT'] == 'Single Pay') {
        Base_CancerArr.push(SinglePay_CancerArr[i]);
      } else {
        Base_CancerArr.push(RegularPay_CancerArr[i]);
      }
    }

    /* Discount calculation */
    var Staff_disc;
    if (this.ProductInputArr['Staff'] == 'Yes')
      if (this.ProductInputArr['PPT'] == 'Single Pay') Staff_disc = 0.02;
      else if (parseInt(this.ProductInputArr['PolicyTerm']) < 10)
        Staff_disc = 0.05;
      else Staff_disc = 0.075;
    else Staff_disc = 0;

    var Online_disc;
    if (Staff_disc == 0 && this.ProductInputArr['SalesChannel'] == 'Online')
      if (this.ProductInputArr['PPT'] == 'Single Pay') Online_disc = 0.01;
      else Online_disc = 0.02;
    else Online_disc = 0;

    var Loyalty_disc;
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      Staff_disc == 0 &&
      this.ProductInputArr['FamilyBenefit'] == 'No' &&
      this.ProductInputArr['LoyaltyBenefit'] == 'Yes'
    )
      Loyalty_disc = 0.05;
    else Loyalty_disc = 0;

    var Family_disc;
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      Staff_disc == 0 &&
      this.ProductInputArr['FamilyBenefit'] == 'Yes'
    )
      Family_disc = 0.05;
    else Family_disc = 0;
    /* Discount calculation */

    /*	Heart Premium	*/
    var Base_FirstPremium;
    var HospitalBenefit_FirstPremium;
    var HospitalBenefit_ShowPremium;

    var IncreasingCoverBenefit_FirstPremium;
    var IncreasingCoverBenefit_ShowPremium;

    var IncomeBenefit_FirstPremium;
    var IncomeBenefit_ShowPremium;

    var Base_SecondPremium;
    var HospitalBenefit_SecondPremium;
    var IncreasingCoverBenefit_SecondPremium;
    var IncomeBenefit_SecondPremium;

    var ComboCoverDiscount = 0;
    /*if(this.ProductInputArr['CoverageOption']=='CancerAndHeart' && this.ProductInputArr['PPT']=='Single Pay')
			ComboCoverDiscount=0.01;
		else if(this.ProductInputArr['CoverageOption']=='CancerAndHeart' && this.ProductInputArr['PPT']=='Regular Pay')
			ComboCoverDiscount=0.02;
		else
			ComboCoverDiscount=0;*/

    if (this.ProductInputArr['LA1_Tobacco'] == 'Yes') {
      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      HospitalBenefit_ShowPremium = HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      IncreasingCoverBenefit_ShowPremium = IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      IncomeBenefit_ShowPremium = IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA1_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        IncomeBenefit_SecondPremium = 0;
      }
      /*	Heart Premium	*/

      /*	Cancer Premium	*/
      var Cancer_Base_FirstPremium;
      var Cancer_HospitalBenefit_FirstPremium;
      var Cancer_IncreasingCoverBenefit_FirstPremium;
      var Cancer_IncomeBenefit_FirstPremium;

      var Cancer_HospitalBenefit_ShowPremium;
      var Cancer_IncreasingCoverBenefit_ShowPremium;
      var Cancer_IncomeBenefit_ShowPremium;

      var Cancer_Base_SecondPremium;
      var Cancer_HospitalBenefit_SecondPremium;
      var Cancer_IncreasingCoverBenefit_SecondPremium;
      var Cancer_IncomeBenefit_SecondPremium;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      Cancer_HospitalBenefit_ShowPremium = Cancer_HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        Cancer_HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      Cancer_IncreasingCoverBenefit_ShowPremium = Cancer_IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      Cancer_IncomeBenefit_ShowPremium = Cancer_IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        Cancer_IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      Cancer_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        Cancer_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        Cancer_HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        Cancer_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA1_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        Cancer_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        Cancer_IncomeBenefit_SecondPremium = 0;
      }
      /*	Cancer Premium	*/
    } else {
      Base_FirstPremium = 0;
      HospitalBenefit_FirstPremium = 0;
      IncreasingCoverBenefit_FirstPremium = 0;
      IncomeBenefit_FirstPremium = 0;
      Base_SecondPremium = 0;
      HospitalBenefit_SecondPremium = 0;
      IncreasingCoverBenefit_SecondPremium = 0;
      IncomeBenefit_SecondPremium = 0;
      Cancer_Base_FirstPremium = 0;
      Cancer_HospitalBenefit_FirstPremium = 0;
      Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      Cancer_IncomeBenefit_FirstPremium = 0;
      Cancer_Base_SecondPremium = 0;
      Cancer_HospitalBenefit_SecondPremium = 0;
      Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      Cancer_IncomeBenefit_SecondPremium = 0;
    }

    /*	LA2 Calculations	*/
    if (
      this.ProductInputArr['PPT'] == 'Regular Pay' &&
      this.ProductInputArr['FamilyBenefit'] == 'Yes' &&
      this.ProductInputArr['LA2_Tobacco'] == 'Yes'
    ) {
      var index_gender;
      if (this.ProductInputArr['LA2_Gender'] == 'Male') {
        index_gender = 0;
      } else {
        index_gender = 1;
      }

      var SinglePay_HeartArr = [];
      var SinglePay_CancerArr = [];
      var RegularPay_HeartArr = [];
      var RegularPay_CancerArr = [];

      var Base_HeartArr = [];
      var Base_CancerArr = [];
      for (var i = 0; i < 4; i++) {
        var VlookupString =
          LA2_Age.toString() +
          this.Slab_Heart_SumAssured(
            this.ProductInputArr['LA2_HeartSumAssured']
          ).toString();
        var SinglePay_HeartBaseRate = this.XRTPremiumRateHeartSP[VlookupString][
          i * 2 + index_gender
        ];
        SinglePay_HeartArr.push(SinglePay_HeartBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.Slab_Cancer_SumAssured(
            this.ProductInputArr['LA2_CancerSumAssured']
          ).toString();
        var SinglePay_CancerBaseRate = this.XRTPremiumRateCancerSP[
          VlookupString
        ][i * 2 + index_gender];
        SinglePay_CancerArr.push(SinglePay_CancerBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.ProductInputArr['PolicyTerm'] +
          this.Slab_HeartSumAssured_RP(
            this.ProductInputArr['LA2_HeartSumAssured']
          ).toString();
        var RegularPay_HeartBaseRate = this.XRTPremiumRateHeartRP[
          VlookupString
        ][i * 2 + index_gender];
        RegularPay_HeartArr.push(RegularPay_HeartBaseRate);

        var VlookupString =
          LA2_Age.toString() +
          this.ProductInputArr['PolicyTerm'] +
          this.Slab_CancerSumAssured_RP(
            this.ProductInputArr['LA2_CancerSumAssured']
          ).toString();
        var RegularPay_CancerBaseRate = this.XRTPremiumRateCancerRP[
          VlookupString
        ][i * 2 + index_gender];
        RegularPay_CancerArr.push(RegularPay_CancerBaseRate);

        if (this.ProductInputArr['PPT'] == 'Single Pay') {
          Base_HeartArr.push(SinglePay_HeartArr[i]);
        } else {
          Base_HeartArr.push(RegularPay_HeartArr[i]);
        }

        if (this.ProductInputArr['PPT'] == 'Single Pay') {
          Base_CancerArr.push(SinglePay_CancerArr[i]);
        } else {
          Base_CancerArr.push(RegularPay_CancerArr[i]);
        }
      }

      /* Discount calculation */
      var Staff_disc;
      if (this.ProductInputArr['Staff'] == 'Yes')
        if (this.ProductInputArr['PPT'] == 'Single Pay') Staff_disc = 0.02;
        else if (parseInt(this.ProductInputArr['PolicyTerm']) < 10)
          Staff_disc = 0.05;
        else Staff_disc = 0.075;
      else Staff_disc = 0;

      var Online_disc;
      if (Staff_disc == 0 && this.ProductInputArr['SalesChannel'] == 'Online')
        if (this.ProductInputArr['PPT'] == 'Single Pay') Online_disc = 0.01;
        else Online_disc = 0.02;
      else Online_disc = 0;

      var Loyalty_disc;
      if (
        Staff_disc == 0 &&
        this.ProductInputArr['FamilyBenefit'] == 'No' &&
        this.ProductInputArr['LoyaltyBenefit'] == 'Yes'
      )
        Loyalty_disc = 0.05;
      else Loyalty_disc = 0;

      var Family_disc;
      if (Staff_disc == 0 && this.ProductInputArr['FamilyBenefit'] == 'Yes')
        Family_disc = 0.05;
      else Family_disc = 0;
      /* Discount calculation */

      /*	Heart Premium	*/
      var LA2_Base_FirstPremium;
      var LA2_HospitalBenefit_FirstPremium;
      var LA2_IncreasingCoverBenefit_FirstPremium;
      var LA2_IncomeBenefit_FirstPremium;

      var LA2_HospitalBenefit_ShowPremium;
      var LA2_IncreasingCoverBenefit_ShowPremium;
      var LA2_IncomeBenefit_ShowPremium;

      var LA2_Base_SecondPremium;
      var LA2_HospitalBenefit_SecondPremium;
      var LA2_IncreasingCoverBenefit_SecondPremium;
      var LA2_IncomeBenefit_SecondPremium;

      var ComboCoverDiscount = 0;
      /*if(this.ProductInputArr['CoverageOption']=='CancerAndHeart' && this.ProductInputArr['PPT']=='Single Pay')
				ComboCoverDiscount=0.01;
			else if(this.ProductInputArr['CoverageOption']=='CancerAndHeart' && this.ProductInputArr['PPT']=='Regular Pay')
				ComboCoverDiscount=0.02;
			else
				ComboCoverDiscount=0;*/

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_HospitalBenefit_ShowPremium = LA2_HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        LA2_HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_IncreasingCoverBenefit_ShowPremium = LA2_IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        LA2_IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_IncomeBenefit_ShowPremium = LA2_IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        LA2_IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_HeartArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_HeartArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_HeartSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_IncomeBenefit_SecondPremium = 0;
      }
      /*	Heart Premium	*/

      /*	Cancer Premium	*/
      var LA2_Cancer_Base_FirstPremium;
      var LA2_Cancer_HospitalBenefit_FirstPremium;
      var LA2_Cancer_IncreasingCoverBenefit_FirstPremium;
      var LA2_Cancer_IncomeBenefit_FirstPremium;

      var LA2_Cancer_HospitalBenefit_ShowPremium;
      var LA2_Cancer_IncreasingCoverBenefit_ShowPremium;
      var LA2_Cancer_IncomeBenefit_ShowPremium;

      var LA2_Cancer_Base_SecondPremium;
      var LA2_Cancer_HospitalBenefit_SecondPremium;
      var LA2_Cancer_IncreasingCoverBenefit_SecondPremium;
      var LA2_Cancer_IncomeBenefit_SecondPremium;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_Base_FirstPremium = PremiumAfterOnlineDiscount_Round;

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[0] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_HospitalBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_HospitalBenefit_ShowPremium = LA2_Cancer_HospitalBenefit_FirstPremium;
      if (this.ProductInputArr['HospitalBenefit'] != 'Yes') {
        LA2_Cancer_HospitalBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[2] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_IncreasingCoverBenefit_ShowPremium = LA2_Cancer_IncreasingCoverBenefit_FirstPremium;
      if (this.ProductInputArr['IncreasingCoverBenefit'] != 'Yes') {
        LA2_Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[3] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterLoyaltyDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Loyalty_disc)
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterLoyaltyDiscount * (1 - Staff_disc)
      );
      var PremiumAfterFamilyBenefit = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 - Family_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterFamilyBenefit * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_IncomeBenefit_FirstPremium = PremiumAfterOnlineDiscount_Round;

      LA2_Cancer_IncomeBenefit_ShowPremium = LA2_Cancer_IncomeBenefit_FirstPremium;
      if (this.ProductInputArr['IncomeBenefit'] != 'Yes') {
        LA2_Cancer_IncomeBenefit_FirstPremium = 0;
      }

      var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
        Base_CancerArr[1] * (1 - ComboCoverDiscount)
      );
      var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
        (PremiumRateAfterComboCoverDiscount *
          parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
        1000
      );
      var PremiumAfterStaffDiscount = this.Trunc_Exel(
        PremiumAfterComboCoverDiscount * (1 - Staff_disc)
      );
      var PremiumAfterModalLoading = this.Trunc_Exel(
        PremiumAfterStaffDiscount * (1 + Modalloadings)
      );
      var PremiumAfterOnlineDiscount = this.Trunc_Exel(
        PremiumAfterModalLoading * (1 - Online_disc)
      );
      var PremiumAfterOnlineDiscount_Round =
        Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
      LA2_Cancer_Base_SecondPremium = PremiumAfterOnlineDiscount_Round;

      if (this.ProductInputArr['HospitalBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[0] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = PremiumRateAfterComboCoverDiscount;
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_HospitalBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_HospitalBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncreasingCoverBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[2] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_IncreasingCoverBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      }

      if (this.ProductInputArr['IncomeBenefit'] == 'Yes') {
        var PremiumRateAfterComboCoverDiscount = this.Trunc_Exel(
          Base_CancerArr[3] * (1 - ComboCoverDiscount)
        );
        var PremiumAfterComboCoverDiscount = this.Trunc_Exel(
          (PremiumRateAfterComboCoverDiscount *
            parseInt(this.ProductInputArr['LA2_CancerSumAssured'])) /
          1000
        );
        var PremiumAfterStaffDiscount = this.Trunc_Exel(
          PremiumAfterComboCoverDiscount * (1 - Staff_disc)
        );
        var PremiumAfterModalLoading = this.Trunc_Exel(
          PremiumAfterStaffDiscount * (1 + Modalloadings)
        );
        var PremiumAfterOnlineDiscount = this.Trunc_Exel(
          PremiumAfterModalLoading * (1 - Online_disc)
        );
        var PremiumAfterOnlineDiscount_Round =
          Math.round(PremiumAfterOnlineDiscount) * FrequencyDiv;
        LA2_Cancer_IncomeBenefit_SecondPremium = PremiumAfterOnlineDiscount_Round;
      } else {
        LA2_Cancer_IncomeBenefit_SecondPremium = 0;
      }
      /*	Cancer Premium	*/
    } else {
      LA2_Base_FirstPremium = 0;
      LA2_HospitalBenefit_FirstPremium = 0;
      LA2_IncreasingCoverBenefit_FirstPremium = 0;
      LA2_IncomeBenefit_FirstPremium = 0;
      LA2_HospitalBenefit_ShowPremium = 0;
      LA2_IncreasingCoverBenefit_ShowPremium = 0;
      LA2_IncomeBenefit_ShowPremium = 0;
      LA2_Base_SecondPremium = 0;
      LA2_HospitalBenefit_SecondPremium = 0;
      LA2_IncreasingCoverBenefit_SecondPremium = 0;
      LA2_IncomeBenefit_SecondPremium = 0;
      LA2_Cancer_Base_FirstPremium = 0;
      LA2_Cancer_HospitalBenefit_FirstPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium = 0;
      LA2_Cancer_IncomeBenefit_FirstPremium = 0;
      LA2_Cancer_HospitalBenefit_ShowPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_ShowPremium = 0;
      LA2_Cancer_IncomeBenefit_ShowPremium = 0;
      LA2_Cancer_Base_SecondPremium = 0;
      LA2_Cancer_HospitalBenefit_SecondPremium = 0;
      LA2_Cancer_IncreasingCoverBenefit_SecondPremium = 0;
      LA2_Cancer_IncomeBenefit_SecondPremium = 0;
    }
    /*	LA2 Calculations	*/
    var output_arr = {};
    output_arr = {
      Base_FirstPremium: Base_FirstPremium,
      HospitalBenefit_FirstPremium: HospitalBenefit_FirstPremium,
      IncreasingCoverBenefit_FirstPremium: IncreasingCoverBenefit_FirstPremium,
      IncomeBenefit_FirstPremium: IncomeBenefit_FirstPremium,
      HospitalBenefit_ShowPremium: HospitalBenefit_ShowPremium,
      IncreasingCoverBenefit_ShowPremium: IncreasingCoverBenefit_ShowPremium,
      IncomeBenefit_ShowPremium: IncomeBenefit_ShowPremium,
      Base_SecondPremium: Base_SecondPremium,
      HospitalBenefit_SecondPremium: HospitalBenefit_SecondPremium,
      IncreasingCoverBenefit_SecondPremium: IncreasingCoverBenefit_SecondPremium,
      IncomeBenefit_SecondPremium: IncomeBenefit_SecondPremium,
      Cancer_Base_FirstPremium: Cancer_Base_FirstPremium,
      Cancer_HospitalBenefit_FirstPremium: Cancer_HospitalBenefit_FirstPremium,
      Cancer_IncreasingCoverBenefit_FirstPremium: Cancer_IncreasingCoverBenefit_FirstPremium,
      Cancer_IncomeBenefit_FirstPremium: Cancer_IncomeBenefit_FirstPremium,
      Cancer_HospitalBenefit_ShowPremium: Cancer_HospitalBenefit_ShowPremium,
      Cancer_IncreasingCoverBenefit_ShowPremium: Cancer_IncreasingCoverBenefit_ShowPremium,
      Cancer_IncomeBenefit_ShowPremium: Cancer_IncomeBenefit_ShowPremium,
      Cancer_Base_SecondPremium: Cancer_Base_SecondPremium,
      Cancer_HospitalBenefit_SecondPremium: Cancer_HospitalBenefit_SecondPremium,
      Cancer_IncreasingCoverBenefit_SecondPremium: Cancer_IncreasingCoverBenefit_SecondPremium,
      Cancer_IncomeBenefit_SecondPremium: Cancer_IncomeBenefit_SecondPremium,
      LA2_Base_FirstPremium: LA2_Base_FirstPremium,
      LA2_HospitalBenefit_FirstPremium: LA2_HospitalBenefit_FirstPremium,
      LA2_IncreasingCoverBenefit_FirstPremium: LA2_IncreasingCoverBenefit_FirstPremium,
      LA2_IncomeBenefit_FirstPremium: LA2_IncomeBenefit_FirstPremium,
      LA2_HospitalBenefit_ShowPremium: LA2_HospitalBenefit_ShowPremium,
      LA2_IncreasingCoverBenefit_ShowPremium: LA2_IncreasingCoverBenefit_ShowPremium,
      LA2_IncomeBenefit_ShowPremium: LA2_IncomeBenefit_ShowPremium,
      LA2_Base_SecondPremium: LA2_Base_SecondPremium,
      LA2_HospitalBenefit_SecondPremium: LA2_HospitalBenefit_SecondPremium,
      LA2_IncreasingCoverBenefit_SecondPremium: LA2_IncreasingCoverBenefit_SecondPremium,
      LA2_IncomeBenefit_SecondPremium: LA2_IncomeBenefit_SecondPremium,
      LA2_Cancer_Base_FirstPremium: LA2_Cancer_Base_FirstPremium,
      LA2_Cancer_HospitalBenefit_FirstPremium: LA2_Cancer_HospitalBenefit_FirstPremium,
      LA2_Cancer_IncreasingCoverBenefit_FirstPremium: LA2_Cancer_IncreasingCoverBenefit_FirstPremium,
      LA2_Cancer_IncomeBenefit_FirstPremium: LA2_Cancer_IncomeBenefit_FirstPremium,
      LA2_Cancer_HospitalBenefit_ShowPremium: LA2_Cancer_HospitalBenefit_ShowPremium,
      LA2_Cancer_IncreasingCoverBenefit_ShowPremium: LA2_Cancer_IncreasingCoverBenefit_ShowPremium,
      LA2_Cancer_IncomeBenefit_ShowPremium: LA2_Cancer_IncomeBenefit_ShowPremium,
      LA2_Cancer_Base_SecondPremium: LA2_Cancer_Base_SecondPremium,
      LA2_Cancer_HospitalBenefit_SecondPremium: LA2_Cancer_HospitalBenefit_SecondPremium,
      LA2_Cancer_IncreasingCoverBenefit_SecondPremium: LA2_Cancer_IncreasingCoverBenefit_SecondPremium,
      LA2_Cancer_IncomeBenefit_SecondPremium: LA2_Cancer_IncomeBenefit_SecondPremium
    };
    return output_arr;
  }
  /**Tax calculation Added */
  calculateEBI(EbiInput, HncEbiOutput) {
    let AllOption_Premiums = [];
    let OutputObjHold = HncEbiOutput;
    let HeartCancer_InputArr = EbiInput;

    let LA1_Heart_1stPremium_WoTax = 0,
      LA1_Heart_1stPremium_WtTax = 0,
      LA1_Heart_RestPremium_WoTax = 0,
      LA1_Heart_RestPremium_WtTax = 0,
      LA1_Cancer_1stPremium_WoTax = 0,
      LA1_Cancer_1stPremium_WtTax = 0,
      LA1_Cancer_RestPremium_WoTax = 0,
      LA1_Cancer_RestPremium_WtTax = 0,
      LA2_Heart_1stPremium_WoTax = 0,
      LA2_Heart_1stPremium_WtTax = 0,
      LA2_Heart_RestPremium_WoTax = 0,
      LA2_Heart_RestPremium_WtTax = 0,
      LA2_Cancer_1stPremium_WoTax = 0,
      LA2_Cancer_1stPremium_WtTax = 0,
      LA2_Cancer_RestPremium_WoTax = 0,
      LA2_Cancer_RestPremium_WtTax = 0;

    if (
      HeartCancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      HeartCancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      HeartCancer_InputArr['CoverageOption'] = 'CancerAndHeart';
    } else if (
      HeartCancer_InputArr['LA1_HeartSumAssured'] == 0 &&
      HeartCancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      HeartCancer_InputArr['CoverageOption'] = 'Cancer';
    } else if (
      HeartCancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      HeartCancer_InputArr['LA1_CancerSumAssured'] == 0
    ) {
      HeartCancer_InputArr['CoverageOption'] = 'Heart';
    }

    if (
      HeartCancer_InputArr['LA2_HeartSumAssured'] != 0 &&
      HeartCancer_InputArr['LA2_CancerSumAssured'] != 0
    ) {
      HeartCancer_InputArr['SpouseCoverageOption'] = 'CancerAndHeart';
    } else if (
      HeartCancer_InputArr['LA2_HeartSumAssured'] == 0 &&
      HeartCancer_InputArr['LA2_CancerSumAssured'] != 0
    ) {
      HeartCancer_InputArr['SpouseCoverageOption'] = 'Cancer cover';
    } else if (
      HeartCancer_InputArr['LA2_HeartSumAssured'] != 0 &&
      HeartCancer_InputArr['LA2_CancerSumAssured'] == 0
    ) {
      HeartCancer_InputArr['SpouseCoverageOption'] = 'Heart cover';
    }

    let Total_Base_PremiumWT = Math.round(
      this.CalculateTax(
      OutputObjHold['Base_FirstPremium'] + 
      OutputObjHold['XRT_Base_FirstPremium'] + 
      OutputObjHold['XRT_Cancer_Base_FirstPremium'] +
      OutputObjHold['Cancer_Base_FirstPremium'] +
      OutputObjHold['LA2_Base_FirstPremium'] +
      OutputObjHold['XRT_LA2_Base_FirstPremium'] +
      OutputObjHold['LA2_Cancer_Base_FirstPremium'] + 
      OutputObjHold['XRT_LA2_Cancer_Base_FirstPremium']
      )
      );

    AllOption_Premiums[
        'Total_Base_PremiumWT'
      ] = Total_Base_PremiumWT;

    if (
      HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
      HeartCancer_InputArr['CoverageOption'] == 'Heart'
    ) {
      LA1_Heart_1stPremium_WoTax =
        Math.round(
          OutputObjHold['Base_FirstPremium'] +
          OutputObjHold['HospitalBenefit_FirstPremium'] +
          OutputObjHold['IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Base_FirstPremium'] +
          OutputObjHold['XRT_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_IncomeBenefit_FirstPremium']
        );
      LA1_Heart_1stPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['Base_FirstPremium'] +
          OutputObjHold['HospitalBenefit_FirstPremium'] +
          OutputObjHold['IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Base_FirstPremium'] +
          OutputObjHold['XRT_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_IncomeBenefit_FirstPremium']
        )
      );
      LA1_Heart_RestPremium_WoTax =
        Math.round(
          OutputObjHold['Base_SecondPremium'] +
          OutputObjHold['HospitalBenefit_SecondPremium'] +
          OutputObjHold['IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Base_SecondPremium'] +
          OutputObjHold['XRT_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_IncomeBenefit_SecondPremium']
        );
      LA1_Heart_RestPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['Base_SecondPremium'] +
          OutputObjHold['HospitalBenefit_SecondPremium'] +
          OutputObjHold['IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Base_SecondPremium'] +
          OutputObjHold['XRT_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_IncomeBenefit_SecondPremium']
        )
      );

      AllOption_Premiums[
        'LA1_Heart_1stPremium_WoTax'
      ] = LA1_Heart_1stPremium_WoTax;
      AllOption_Premiums[
        'LA1_Heart_1stPremium_WtTax'
      ] = LA1_Heart_1stPremium_WtTax;
      AllOption_Premiums[
        'LA1_Heart_RestPremium_WoTax'
      ] = LA1_Heart_RestPremium_WoTax;
      AllOption_Premiums[
        'LA1_Heart_RestPremium_WtTax'
      ] = LA1_Heart_RestPremium_WtTax;
    }

    if (
      HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
      HeartCancer_InputArr['CoverageOption'] == 'Cancer'
    ) {
      LA1_Cancer_1stPremium_WoTax =
        Math.round(
          OutputObjHold['Cancer_Base_FirstPremium'] +
          OutputObjHold['Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['Cancer_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Cancer_Base_FirstPremium'] +
          OutputObjHold['XRT_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_Cancer_IncomeBenefit_FirstPremium']
        );
      LA1_Cancer_1stPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['Cancer_Base_FirstPremium'] +
          OutputObjHold['Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['Cancer_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Cancer_Base_FirstPremium'] +
          OutputObjHold['XRT_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_Cancer_IncomeBenefit_FirstPremium']
        )
      );
      LA1_Cancer_RestPremium_WoTax =
        Math.round(
          OutputObjHold['Cancer_Base_SecondPremium'] +
          OutputObjHold['Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['Cancer_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Cancer_Base_SecondPremium'] +
          OutputObjHold['XRT_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_Cancer_IncomeBenefit_SecondPremium']
        );
      LA1_Cancer_RestPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['Cancer_Base_SecondPremium'] +
          OutputObjHold['Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['Cancer_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_Cancer_Base_SecondPremium'] +
          OutputObjHold['XRT_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_Cancer_IncomeBenefit_SecondPremium']
        )
      );

      AllOption_Premiums[
        'LA1_Cancer_1stPremium_WoTax'
      ] = LA1_Cancer_1stPremium_WoTax;
      AllOption_Premiums[
        'LA1_Cancer_1stPremium_WtTax'
      ] = LA1_Cancer_1stPremium_WtTax;
      AllOption_Premiums[
        'LA1_Cancer_RestPremium_WoTax'
      ] = LA1_Cancer_RestPremium_WoTax;
      AllOption_Premiums[
        'LA1_Cancer_RestPremium_WtTax'
      ] = LA1_Cancer_RestPremium_WtTax;
    }

    /*	LA2 Display	*/
    if (
      HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
      HeartCancer_InputArr['CoverageOption'] == 'Heart'
    ) {
      LA2_Heart_1stPremium_WoTax =
        Math.round(
          OutputObjHold['LA2_Base_FirstPremium'] +
          OutputObjHold['LA2_HospitalBenefit_FirstPremium'] +
          OutputObjHold['LA2_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['LA2_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Base_FirstPremium'] +
          OutputObjHold['XRT_LA2_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_LA2_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_LA2_IncomeBenefit_FirstPremium']
        );
      LA2_Heart_1stPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['LA2_Base_FirstPremium'] +
          OutputObjHold['LA2_HospitalBenefit_FirstPremium'] +
          OutputObjHold['LA2_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['LA2_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Base_FirstPremium'] +
          OutputObjHold['XRT_LA2_HospitalBenefit_FirstPremium'] +
          OutputObjHold['XRT_LA2_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['XRT_LA2_IncomeBenefit_FirstPremium']
        )
      );
      LA2_Heart_RestPremium_WoTax =
        Math.round(
          OutputObjHold['LA2_Base_SecondPremium'] +
          OutputObjHold['LA2_HospitalBenefit_SecondPremium'] +
          OutputObjHold['LA2_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['LA2_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Base_SecondPremium'] +
          OutputObjHold['XRT_LA2_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_LA2_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_LA2_IncomeBenefit_SecondPremium']
        );
      LA2_Heart_RestPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['LA2_Base_SecondPremium'] +
          OutputObjHold['LA2_HospitalBenefit_SecondPremium'] +
          OutputObjHold['LA2_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['LA2_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Base_SecondPremium'] +
          OutputObjHold['XRT_LA2_HospitalBenefit_SecondPremium'] +
          OutputObjHold['XRT_LA2_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['XRT_LA2_IncomeBenefit_SecondPremium']
        )
      );

      AllOption_Premiums[
        'LA2_Heart_1stPremium_WoTax'
      ] = LA2_Heart_1stPremium_WoTax;
      AllOption_Premiums[
        'LA2_Heart_1stPremium_WtTax'
      ] = LA2_Heart_1stPremium_WtTax;
      AllOption_Premiums[
        'LA2_Heart_RestPremium_WoTax'
      ] = LA2_Heart_RestPremium_WoTax;
      AllOption_Premiums[
        'LA2_Heart_RestPremium_WtTax'
      ] = LA2_Heart_RestPremium_WtTax;
    }
    if (
      HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
      HeartCancer_InputArr['CoverageOption'] == 'Cancer'
    ) {
      LA2_Cancer_1stPremium_WoTax =
        Math.round(
          OutputObjHold['LA2_Cancer_Base_FirstPremium'] +
          OutputObjHold['LA2_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['LA2_Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['LA2_Cancer_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Cancer_Base_FirstPremium'] +
          OutputObjHold['XRT_LA2_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold[
          'XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium'
          ] +
          OutputObjHold['XRT_LA2_Cancer_IncomeBenefit_FirstPremium']
        );
      LA2_Cancer_1stPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['LA2_Cancer_Base_FirstPremium'] +
          OutputObjHold['LA2_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold['LA2_Cancer_IncreasingCoverBenefit_FirstPremium'] +
          OutputObjHold['LA2_Cancer_IncomeBenefit_FirstPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Cancer_Base_FirstPremium'] +
          OutputObjHold['XRT_LA2_Cancer_HospitalBenefit_FirstPremium'] +
          OutputObjHold[
          'XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium'
          ] +
          OutputObjHold['XRT_LA2_Cancer_IncomeBenefit_FirstPremium']
        )
      );
      LA2_Cancer_RestPremium_WoTax =
        Math.round(
          OutputObjHold['LA2_Cancer_Base_SecondPremium'] +
          OutputObjHold['LA2_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['LA2_Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['LA2_Cancer_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Cancer_Base_SecondPremium'] +
          OutputObjHold['XRT_LA2_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold[
          'XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium'
          ] +
          OutputObjHold['XRT_LA2_Cancer_IncomeBenefit_SecondPremium']
        );
      LA2_Cancer_RestPremium_WtTax = this.CalculateTax(
        Math.round(
          OutputObjHold['LA2_Cancer_Base_SecondPremium'] +
          OutputObjHold['LA2_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold['LA2_Cancer_IncreasingCoverBenefit_SecondPremium'] +
          OutputObjHold['LA2_Cancer_IncomeBenefit_SecondPremium']
        ) +
        Math.round(
          OutputObjHold['XRT_LA2_Cancer_Base_SecondPremium'] +
          OutputObjHold['XRT_LA2_Cancer_HospitalBenefit_SecondPremium'] +
          OutputObjHold[
          'XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium'
          ] +
          OutputObjHold['XRT_LA2_Cancer_IncomeBenefit_SecondPremium']
        )
      );

      AllOption_Premiums[
        'LA2_Cancer_1stPremium_WoTax'
      ] = LA2_Cancer_1stPremium_WoTax;
      AllOption_Premiums[
        'LA2_Cancer_1stPremium_WtTax'
      ] = LA2_Cancer_1stPremium_WtTax;
      AllOption_Premiums[
        'LA2_Cancer_RestPremium_WoTax'
      ] = LA2_Cancer_RestPremium_WoTax;
      AllOption_Premiums[
        'LA2_Cancer_RestPremium_WtTax'
      ] = LA2_Cancer_RestPremium_WtTax;
    }
    /*	LA2 Display	*/

    AllOption_Premiums['LA1_first_premium'] =
      LA1_Heart_1stPremium_WtTax + LA1_Cancer_1stPremium_WtTax;
    AllOption_Premiums['LA2_first_premium'] =
      LA2_Heart_1stPremium_WtTax + LA2_Cancer_1stPremium_WtTax;

    /*popup*/
    /*	Regular Pay	*/
    if (this.RegularPayFlag && !this.frequencyYearlyFlag) {
      this.RegularPayFlag = false;
      var temp = JSON.parse(JSON.stringify(HeartCancer_InputArr));
      temp['PPT'] = 'Regular Pay';
      temp['LoyaltyBenefit'] = 'No';
      temp['FamilyBenefit'] = 'No';
      OutputObjHold = this.HeartCancerEBI(temp);
      LA1_Heart_1stPremium_WtTax = 0;
      LA1_Cancer_1stPremium_WtTax = 0;
      if (
        HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
        HeartCancer_InputArr['CoverageOption'] == 'Heart'
      ) {
        LA1_Heart_1stPremium_WtTax = this.CalculateTax(
          Math.round(
            OutputObjHold['Base_FirstPremium'] +
            OutputObjHold['HospitalBenefit_FirstPremium'] +
            OutputObjHold['IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['IncomeBenefit_FirstPremium']
          ) +
          Math.round(
            OutputObjHold['XRT_Base_FirstPremium'] +
            OutputObjHold['XRT_HospitalBenefit_FirstPremium'] +
            OutputObjHold['XRT_IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['XRT_IncomeBenefit_FirstPremium']
          )
        );
      }
      if (
        HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
        HeartCancer_InputArr['CoverageOption'] == 'Cancer'
      ) {
        LA1_Cancer_1stPremium_WtTax = this.CalculateTax(
          Math.round(
            OutputObjHold['Cancer_Base_FirstPremium'] +
            OutputObjHold['Cancer_HospitalBenefit_FirstPremium'] +
            OutputObjHold['Cancer_IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['Cancer_IncomeBenefit_FirstPremium']
          ) +
          Math.round(
            OutputObjHold['XRT_Cancer_Base_FirstPremium'] +
            OutputObjHold['XRT_Cancer_HospitalBenefit_FirstPremium'] +
            OutputObjHold[
            'XRT_Cancer_IncreasingCoverBenefit_FirstPremium'
            ] +
            OutputObjHold['XRT_Cancer_IncomeBenefit_FirstPremium']
          )
        );
      }
      this.frequencyYearlyFlag = true;
      var addition_premiums =
        LA1_Heart_1stPremium_WtTax + LA1_Cancer_1stPremium_WtTax;
      AllOption_Premiums['LA1_first_premium_regularpay'] = addition_premiums;
    }

    /*	Datalayer	*/
    if (!this.RegularPayFlag && this.frequencyYearlyFlag) {
      this.frequencyYearlyFlag = false;
      var temp = JSON.parse(JSON.stringify(HeartCancer_InputArr));
      temp['Frequency'] = 'Yearly';
      OutputObjHold = this.HeartCancerEBI(temp);
      LA1_Heart_1stPremium_WtTax = 0;
      LA1_Cancer_1stPremium_WtTax = 0;
      if (
        HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
        HeartCancer_InputArr['CoverageOption'] == 'Heart'
      ) {
        LA1_Heart_1stPremium_WtTax = this.CalculateTax(
          Math.round(
            OutputObjHold['Base_FirstPremium'] +
            OutputObjHold['HospitalBenefit_FirstPremium'] +
            OutputObjHold['IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['IncomeBenefit_FirstPremium']
          ) +
          Math.round(
            OutputObjHold['XRT_Base_FirstPremium'] +
            OutputObjHold['XRT_HospitalBenefit_FirstPremium'] +
            OutputObjHold['XRT_IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['XRT_IncomeBenefit_FirstPremium']
          )
        );
      }
      if (
        HeartCancer_InputArr['CoverageOption'] == 'CancerAndHeart' ||
        HeartCancer_InputArr['CoverageOption'] == 'Cancer'
      ) {
        LA1_Cancer_1stPremium_WtTax = this.CalculateTax(
          Math.round(
            OutputObjHold['Cancer_Base_FirstPremium'] +
            OutputObjHold['Cancer_HospitalBenefit_FirstPremium'] +
            OutputObjHold['Cancer_IncreasingCoverBenefit_FirstPremium'] +
            OutputObjHold['Cancer_IncomeBenefit_FirstPremium']
          ) +
          Math.round(
            OutputObjHold['XRT_Cancer_Base_FirstPremium'] +
            OutputObjHold['XRT_Cancer_HospitalBenefit_FirstPremium'] +
            OutputObjHold[
            'XRT_Cancer_IncreasingCoverBenefit_FirstPremium'
            ] +
            OutputObjHold['XRT_Cancer_IncomeBenefit_FirstPremium']
          )
        );
      }
      this.RegularPayFlag = true;
      var addition_premiums =
        LA1_Heart_1stPremium_WtTax + LA1_Cancer_1stPremium_WtTax;
      AllOption_Premiums['LA1_AnnualPremium'] = addition_premiums;
    }
    return AllOption_Premiums;
  }
  CalculateTax(Premium) {
    //Premium = Premium+Math.round(Premium*0.14)+Math.round(Premium*0.005)+Math.round(Premium*0.005);
    if (Premium != 0) {
      Premium = Premium + Math.round(Premium * 0.18) + 1;
    } else {
      Premium = Premium + Math.round(Premium * 0.18);
    }
    return Premium;
  }
  CalculateBenefitTax(Premium) {
    if (Premium != 0) {
      Premium = Premium + Math.round(Premium * 0.18);
    } else {
      Premium = Premium + Math.round(Premium * 0.18);
    }
    return Premium;
  }
}

