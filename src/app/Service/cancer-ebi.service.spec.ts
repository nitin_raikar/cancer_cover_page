import { TestBed, inject } from '@angular/core/testing';

import { CancerEbiService } from './cancer-ebi.service';

describe('CancerEbiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CancerEbiService]
    });
  });

  it('should be created', inject([CancerEbiService], (service: CancerEbiService) => {
    expect(service).toBeTruthy();
  }));
});
