import { Component, OnInit } from '@angular/core';
import { TempServiceService } from 'src/app/Service/temp-service.service';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css']
})
export class BasicDetailsComponent implements OnInit {
  dobErrorMsg;

  constructor(public tempservice: TempServiceService) { }

  ngOnInit() {
  }

  // function to move into personalPage
  nextPage() {
    this.tempservice.flag['showHide']['personalPage'] = true;
  }

  backToQuote() {
    this.tempservice.showHideMobile = false;
    this.tempservice.flag['showHide']['quoteSection'] = true;
  }

  // function to check Dob
  onKeyPress(evt, fieldID) {
    const fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tempservice.flag['showHide']['disableBtn'] = false;
    } else {
      this.tempservice.flag['showHide']['disableBtn'] = true;
    }
  }
}
